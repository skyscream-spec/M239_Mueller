# Anforderungen

## Inhaltsverszeichnis
- [1. Übersicht](#Übersicht)
- [2. Die Firma](#Umfang)
- [3. Umfang](#Umfang)
- [4. TBZ Maas Cloud](#TBZ Maas Cloud)
- [5. Ziele](#Ziele)

### Übersicht
In diesem Projekt haben wir den Auftrag bekommen, eine Firma zu gründen und für diese Firma einen Webauftritt und andere Services einzurichten. Spezifisch heisst das:
- Webauftritt (Website)
- Mailserver
- Weitere Services wie Webchat, FTP oder Filesystem

### Die Firma
- Name: Cube Team Solutions
- Angebot: Evaluieren und Implementieren von Micro-Services
- Website: www.tcsolutions.ch
- Mail: contact@tcsolutions.ch

Ich habe mich für diese Firma entschieden, da dies auf mein Endprodukt hindeuted.  

### Umfang 
Ich möchte diese Umgebung auf Kubernetetes realisieren, genauer gesagt auf dem TBZ Maas System. Ich habe mich für Kubernetes entschieden da dies mir nach erfolgreicher Installation, eine einfache Anpassung ermöglicht. 
Ich werde erstellen Container erstellen, diese werden für die einzelnen Services verantwortlich sind. 
- Container 1 : Webserver
- Container 2 : Mailserver
- Container 3 : Chatserver

Des weitern werden möglicherweise noch Container benötigt für Datenbanken oder benötigte Services um die Umgebung zu überwachen.

### TBZ Maas Cloud

Ich habe mich für das TBZ Maas entschieden, weil dies meine Infrastruktur Zuhause entlasted. Zusätlich habe ich über WireGuard die Möglichkeit von überallher auf mein System zugreiffen zukönnen. 
Ich kann durch diese Wahl sämtliche Kosten auf die TBZ abwälzen kann und mein System somit eine gesicherte Uptime habe. 

Auflistung
- Infrastruktur entlasten
- Verfügbarkeit von Extern
- Keine Kosten
- Gesicherte Uptime

### Ziele 
1. Mit kubernetes eine Containerumgebung einrichten. 
2. Web, Mail und Chat Server auf dieser Containerumgebung realisieren.
