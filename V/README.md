### Sicherheitskonzept

| Iso Layer | Was          | Massnahme                                       |
|-----------|--------------|-------------------------------------------------|
| 8         | Anwender     | Sicherheitsschulungen/ Cyber-Resilienz Schulung |
| 7         | Application  | Benutzerauth.                                   |
| 6         | Presentation | Zertifikate                                     |
| 5         | Session      |                                                 |
| 4         | Transport    |                                                 |
| 3         | Network      | IDS und IPS                                     |
| 2         | Data Link    | VLan's,                                         |
| 1         | Physical     | Zugangskontrolle (Serveranlagen)                |

|Was?                              |Sicherung                                                                                  |
|----------------------------------|-------------------------------------------------------------------------------------------|
|    Sensibilisierung Mitarbeiter  |   Mitarbeiter über Regelung informieren, Regelmässige Schulung (Phising, etc.)            |
|    Datensicherungerung           |   Backup Konzept                                                                          |
|    Schadprogramme                |   neuste Updates und Patches installieren, Mail filter, Zertifikate                       |
|    Regelung Hardware             |   Keine Admin Rechte auf den Hardwares, Redundanz der Harware, Ausfallsicherung           |
|    Regelung Software             |   Keine Änderungsrechte bei Interenen Softwares, Keine Rechte Softwares zu installieren   |
|    Büro                          |   Abgeschlossene Räume, Password Richtlinien                                              |
|    Netzwerke                     |   Firewall und Router möglichst wenig Ports, VLAN'S oder Subnetze                         |
|    Internet                      |   Internet nur für Geschäftsangelegenheiten                                               |

### Gefahren und Risiken 

|Risiken|Massnahmen|
|--------|--------|
|    Hacking    |    Sicherung auf allen Layers, Schulung von Mitarbeiter, Zertifikate |
|    Internet Ausfall   |     Vetrag mit Internetanbieter, meherere ISP, redundanz |
|   Strom Ausfall |    USV zur Überbrückung, Ausfallsicherungengen, verschiedene Sromanbieter |
|   Datenverlust   |    Backup System/konzept, Verantwortlichkeiten  |


### Backup
Die Sicherungen findet jeden Tag um 0:00 Uhr statt. Die Backupinfrastruktur ist so ausgelegt, dass am Samstag's Full backup und Sonntags-Freitags Incremental Backup Sicherungen erstellt.

| Montag     | Dienstag     | Mittwoch     | Donnestag    | Freitag      | Samstag | Sonntag      |
|------------|--------------|--------------|--------------|--------------|---------|--------------|
| Incrementl | Incrementell | Incrementell | Incrementell | Incrementell | Full    | Incrementell |
