# Know-How & Begriffe

## 1-Wie kann derselbe webserver unterschiedliche Websites hosten?  
Der Webserver teilt die Rechen ressuorcen des Host servers mit den Websites. All diese websites werden anschliessend auf einer IP Addresse errechbar sein.

## 2- Wie funktioniert DNS? Welche "Ressource Record"-Typen gibt es? Wozu gibt es diese? 
Der DNS Server ist wie ein telefonbuch. erspeichert den Domain name und die IP Addressen aller Websites (root). Der DNS-Server übersetzen Anfragen in IP-Adressen und steuern so, welchen Server ein User erreicht, wenn er eine Domäne in seinen Webbrowser eingibt. DNS ist auf der Anwendungsschicht des OSI-Schichtenmodells angeordnet. 

- A: Ipv4 Adressen
- AAAA: IPv6 Adressen
- CAA: Liste der cert providers für eine Websiste
- MX: Mail Exchange

### Rekursion
Ein Client sendet eine Anfrage an den resovler, wenn dieser die Anfrage nicht auflösen kann, dann wird es weitergereicht an den nächsten DNS Server. Dies geschieht, so lange bis die Adresse aufgelöst werden konnte. 

### Iteration
Die iterative Namensauflösung erfolgt meist zwischen DNS-Servern.
Der zuständige DNS-Server übergibt seinen DNS-Request an einen in der Hierarchie höher stehenden DNS-Server . Der beantwortet den DNS-Request mit einem Verweis auf andere Nameserver, die den Namen auflösen können (z. B. autoritative Nameserver). Der DNS-Server muss sich dann um einen erneuten DNS-Request beim autoriative Nameserver kümmern, bis der Domain-Name vollständig aufgelöst ist.

## Wie können Angaben über einen beliebigen fremden Server herausgefunden werden?
- DNS Dumpfiles: Es gibt Webistes welche sich die Domänadressen zunutzemahcen und zugehörige Services finden. -> https://dnsdumpster.com/
- NS Lookup: Frag den DNS Server nach informationen zu dem Server -> https://www.nslookup.io/
- Winver: Zeigt einem welche Version von Windows ich besitze. 
- Appwiz.cpl: Löschen und Hinzufügen von Programmen 
- IP Config: Ip Adresse herausfinden.

## Was ist ein Proxy? Welche Funktionen übernimmt er?
Der proxy fungiert als Stellvertreter. Er nimmt die anfrage eines Clients oder Server an und Leited diese mit seiner eigenen IP Adresse weiter. Bei dieser Art der Kommunikation besteht keine direkte Verbindung zwischen Absender und Empfänger. Die Hauptfunktionen eines Proxies sind Verschleiern, Absichern und Beschleunigen von Datenübertragungen. 
Der Proxy Filtert und Schütz das netzwerk dahinter. 

## Was ist eine DMZ? Wozu dient sie?
In Computer-Netzwerken bezeichnet DMZ, Hosts oder kleinen Netzwerke, die eine „neutrale Zone“ zwischen dem privaten Netzwerk eines Unternehmens und externen öffentlichen Netzwerken bilden. Sie verhindern den direkten Zugriff auf einen Server mit Unternehmensdaten durch externe Nutzer. Meisst werden Server, welche nach aussen Exposed sind in eine DMZ Gesteckt. für diese Server ist es weniger schlimm wenn sie angegriffen werden. 
