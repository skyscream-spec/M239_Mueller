# Zugriffsberechtigung 

## Benutzerkonzept

Ich unterteile die berechtigungen in Lesen, Schreiben und Löschen.

|                   | Daten  | GL | Buchhaltung |
|-------------------|--------|-------|-------|
|Admin              |  Voll  | Voll| Voll|
|Geschäftführer     |  Voll | Voll | Voll|
|Verkauf Mitarbeiter|  Voll  | \ | \
|Buchhaltung        | Voll   | \ | Voll 
|Praktikant         | Alles auser Löschen    | \ | \ | \ |

### Webserver

Administrator: besitzt Vollzugriff 

Kunde: Homepage

Mitarbeiter: Homepage

# Umsetzung 

Ich würde verschiedenene usergroups erstellen welche die Einzenen user beinhalten. Diese user gruppen bekommen nur auf die Ihnen vorbestimmten daten Zugriff. 
