### Was nutze ich für den Mail Server?

Ich nutze das Paket Mailcow, es verbindet Postfix und Dovecot plus einen Webmailer in ein Paket und bietet ein Gui an.

### Installation 
1. Port Kontrolle -> Sind alle Ports offen welche Mailcow benutzt?
> ss -tlpn | grep -E -w '25|80|110|143|443|465|587|993|995|4190'

![image.png](./image.png)
Dies habe ich als Antwort bekommen (Wird benutzt von meinem Webserver, ich werde diesen Port später unter mailcow.conf ändern)¨

2. Ich überprüfte unabhängig vom Installatiosprozess, ob ich die UFW Firewall aktiviert hatte. 
![image-1.png](./image-1.png)

3. Als nächster schritt habe ich das Network Time Protokoll aktivieren müssen.
![image-2.png](./image-2.png)
Sowie anpassungen im .conf file vornehemen 
![image-3.png](./image-3.png)

4. Nun istallierte ich die Docker engine. 
> curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
> apt install docker-ce docker-ce-cli -y

![image-5.png](./image-5.png)

5. Nächster schritt war das Docker compose des Mailcow Packetes
> curl -L https://github.com/docker/compose/releases/download/$(curl -Ls https://www.servercow.de/docker-compose/latest.php)/docker-compose-$(uname -s)-$(uname -m) > /usr/local/bin/docker-compose

6. Nun Laden wir die Docker instanz von Github herunter. 
>  git clone https://github.com/mailcow/mailcow-dockerized
![image-6.png](./image-6.png)

7. Schritt sieben brachte mich zum erstellen des Conf files für die Mailcow
![image-7.png](./image-7.png)

8. 

### Probleme 
![image-8.png](./image-8.png)
Der Speicher war schnell gefüllt

Ich bin gegen das Problem gelaufen, dass der server der TBZ nur 7 GB an speicher besitz ich aber 20 benötige für dieses Paket.


