### Webserver installieren 
Vorgehensweise 

1. Das Index files der bestehendenden Website löschen 
> sudo rm index.html

2. Hoch laden des html codes über file zilla > /home/ubuntu/

3. Löschen des Folders /var/www/html 
> sudo rm -rf /var/www/html

4. Umbenennen des html codes 
> mv Site1/ html/

5. Verschieben des ordners nach > /var/www
> sudo mv /html > /var/www

6. Fertig
