### ICR Chat
Internet Relay Chat, oder kurz IRC, ist ein textbasiertes Chatsystem. Es kann mehrere Gesprächsrunden mit beliebig vielen Teilnehmern in sogenannten Gesprächskanälen („Kanälen“) führen, aber auch mit nur zwei Gesprächspartnern sprechen.Jeder Teilnehmer kann einen neuen Kanal öffnen, und Sie können auch gleichzeitig an mehreren Kanalgesprächen teilnehmen.

### Installation
1. herunterladen des Paketes
![image.png](./image.png)

2. Password verschlüsseln 
![image-1.png](./image-1.png)

3. Bearbeiten des conf file
![image-2.png](./image-2.png)

- servername

![image-3.png](./image-3.png)

- operator user 

![image-4.png](./image-4.png)

- weitere

4. Verbinden mit einem IRC Client 
![image-5.png](./image-5.png)
![image-6.png](./image-6.png)

5. Happy Chat

![image-7.png](./image-7.png)
![image-10.png](./image-10.png)
![image-9.png](./image-9.png)
